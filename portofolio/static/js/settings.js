particlesJS("particles-js",
    {
        "particles":
        {
            "number":
            {
                "value": 30,
                "density": { "enable": true, "value_area": 800 }
            },
            "size": {
                "value": 3,
                "random": true,
                "anim": {
                    "enable": false,
                    "speed": 4,
                    "size_min": 0.3,
                    "sync": false
                }
            },
            "move": {
                "enable": true,
                "speed": 0.5,
                "random": true
            }
        },
    });